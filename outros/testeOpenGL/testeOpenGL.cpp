#include <stdio.h>

#include <freeglut.h>
#include <math.h>
#include <stdio.h>

// posicao da camera em coordenadas esfericas
float r = 100, theta = 0, phi = 0;
// direcao da camera
float lx, ly, lz;
// posicao da camera
float x = -10.0f, y = 10.0f, z = -10.0f;

// variacao da posicao do mouse
float xOrigin = 0, yOrigin = 0;


float dTheta = 0, dPhi = 0;
float dist = 0;

void computePos(float deltaMove)
{
	theta += dTheta;
	if (theta >= 3.1415)
	{
		theta = 3.1415;
	}

	else if (theta < -3.1415)
	{
		theta = -3.1415;
	}

	if (theta == 3.1415)
		printf("ro: %f, theta: %f, phi: %f", &r, &theta, &phi);

	phi += dPhi;
	if (phi > 3.1415)
		phi = 3.1415;
	if (phi < -3.1415)
		phi = -3.1415;

	r += dist*3.0f;
	if (r < 8)
		r = 8;

	x = r * sin(theta) * cos(phi);
	y = r * sin(theta) * sin(phi);
	z = r * cos(theta);

	lx = -x;
	ly = -y;
	lz = -z;
}

void renderScene(void) {

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();

	computePos(dist);

	// Set the camera

	gluLookAt(x, y, z,
		lx, ly, lz,
		sin(phi), cos(phi), 1);

	glColor4f(1, 0, 0, 0.6);
	glBegin(GL_TRIANGLES);
	glVertex3f(1, 1, 1);
	glVertex3f(1, 0, 0);
	glVertex3f(-1, 0, -2);
	glEnd();

	glLineWidth(2.5);
	glColor4f(0.0, 1.0, 0.0, 1);
	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(15, 1, 15);
	glEnd();

	glLineWidth(2.5);
	glColor4f(0.0, 1.0, 0.0, 1);
	glBegin(GL_LINES);
	glVertex3f(15, 1, 15);
	glVertex3f(0.0, 0.0, 0.0);
	glEnd();

	glColor4f(1, 0, 0, 0.6);
	glBegin(GL_TRIANGLES);
	glVertex3f(1,2,0);
	glVertex3f(1, 2, 2);
	glVertex3f(1, 0, 2);
	glEnd();



	glutSwapBuffers();
}

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	if (h == 0)
		h = 1;
	float ratio = 1.0* w / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45, ratio, 1, 1000);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void pressKey(int key, int x, int y) 
{

	switch (key) 
	{
		case GLUT_KEY_UP: dPhi = -0.1f; break;
		case GLUT_KEY_DOWN: dPhi = +0.1f; break;
		case GLUT_KEY_LEFT: dTheta = 0.1f; break;
		case GLUT_KEY_RIGHT: dTheta = -0.1f; break;
	}
}

void releaseKey(int key, int x, int y)
{
	switch (key) 
	{
		case GLUT_KEY_UP: dPhi = 0; break;
		case GLUT_KEY_DOWN: dPhi = 0; break;
		case GLUT_KEY_LEFT: dTheta = 0; break;
		case GLUT_KEY_RIGHT: dTheta = 0; break;
	}
}

void mouseButton(int button, int state, int xx, int yy) {


	if (button == GLUT_LEFT_BUTTON) 
	{

		// when the button is released
		if (state == GLUT_UP) 
		{
			dTheta = 0;
			dPhi = 0;
		}

		else
		{
			xOrigin = xx;
			yOrigin = yy;
		}
	}
}

void mouseMove(int xx, int yy) {

	if (xOrigin > 0) 
	{
		dTheta = (xx - xOrigin) * 0.001f;
	}
	else
	{
		dTheta = 0;
	}

	if (yOrigin > 0)
	{
		dPhi = (yy - yOrigin) * 0.001f;
	}
	else
	{
		dPhi = 0;
	}
}

int main(int argc, char **argv) 
{

	// init GLUT and create Window
	//glutInit(&argc, argv);
	getchar();
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	getchar();
	glutInitWindowPosition(100, 100);
	getchar();
	glutInitWindowSize(1200, 800);
	getchar();
	glutCreateWindow("testeOpenGL");



	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);
	glutSpecialFunc(pressKey);
	glutSpecialUpFunc(releaseKey);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMove);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}