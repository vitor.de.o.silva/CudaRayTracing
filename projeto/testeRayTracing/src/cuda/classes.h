#ifndef classes_H
#define classes_H

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

extern int MAX_THREADSPERBLOCK;

extern int N_REFLEXOES;
extern int N_RAIOS;
extern int N_TRIANGULOS;

class Vec3f
{
public:
	float x, y, z;

	__device__ Vec3f();
	Vec3f(float a, float b, float c);

	__device__ Vec3f operator-(const Vec3f& b);
	__device__ Vec3f operator+(const Vec3f& b);
	__device__ Vec3f operator*(const float& b);
};

class Raio
{
public:
	Vec3f origem, dir;
	int ultimo;

	Raio();
	 Raio(Vec3f origem, Vec3f direcao);
};

class Plano
{
public:
	Vec3f vetor1, vetor2, normal;
	float a, b, c, d;

	 Plano();
	 Plano(Vec3f vetor1, Vec3f vetor2, Vec3f ponto);
};

class Triangulo
{
public:
	Vec3f a, b, c;
	Plano plano;

	 Triangulo();
	 Triangulo(Vec3f ponto1, Vec3f ponto2, Vec3f ponto3);
};


 Vec3f normaliza(Vec3f v);
 Vec3f prodVetorial(Vec3f v1, Vec3f v2);
 __device__  Vec3f prodVetorial_d(Vec3f v1, Vec3f v2);
 float modulo(Vec3f v);
 float prodEscalar(Vec3f v1, Vec3f v2);
 __device__ float prodEscalar_d(Vec3f v1, Vec3f v2);
 __device__ bool sinal(float v);

 //-----------variaveis globais-----------//
 extern Raio *raios_h, *raios_d;
 extern Triangulo *triangulos_h, *triangulos_d;
 extern Vec3f *posicoesReflexao_d;
 extern int reflexoes_h[1], *reflexoes_d;

#endif
