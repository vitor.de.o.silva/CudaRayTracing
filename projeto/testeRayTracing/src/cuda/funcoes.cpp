#include "classes.h"
#include <stdio.h>
#include <math.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
//-----------------------------CONSTRUTORES--------------------------------

Vec3f::Vec3f(float a, float b, float c)
{
	Vec3f::x = a;
	Vec3f::y = b;
	Vec3f::z = c;
}
Raio::Raio() {};

Raio::Raio(Vec3f origem, Vec3f direcao)
{
	Raio::origem = origem;
	Raio::dir = normaliza(direcao);
	Raio::ultimo = -1;
}

Plano::Plano() {};

Plano::Plano(Vec3f vetor1, Vec3f vetor2, Vec3f ponto)
{
	Plano::vetor1 = vetor1;
	Plano::vetor2 = vetor2;

	normal = normaliza(prodVetorial(vetor1, vetor2));
	a = normal.x;
	b = normal.y;
	c = normal.z;
	d = prodEscalar(normal, ponto);
}

Triangulo::Triangulo() {};

Triangulo::Triangulo(Vec3f ponto1, Vec3f ponto2, Vec3f ponto3)
{
	a = ponto1;
	b = ponto2;
	c = ponto3;

	Vec3f ab, ac;
	ab.x = b.x - a.x;
	ab.y = b.y - a.y;
	ab.z = b.z - a.z;

	ac.x = c.x - a.x;
	ac.y = c.y - a.y;
	ac.z = c.z - a.z;

	Triangulo::plano = Plano(ab, ac, a);
}

//---------------------------FUNCOES---------------------------------------

Vec3f normaliza(Vec3f v)
{
	float norma = modulo(v);

	Vec3f out;

	out.x = v.x / norma;
	out.y = v.y / norma;
	out.z = v.z / norma;

	return out;
}

float modulo(Vec3f v)
{
	float out = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	return out;
}

float prodEscalar(Vec3f v1, Vec3f v2)
{
	float out;

	out = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;

	return out;
}


Vec3f prodVetorial(Vec3f v1, Vec3f v2)
{
	Vec3f out;

	out.x = v1.y*v2.z - v1.z*v2.y;
	out.y = v1.z*v2.x - v1.x*v2.z;
	out.z = v1.x*v2.y - v1.y*v1.x;

	return out;
}

