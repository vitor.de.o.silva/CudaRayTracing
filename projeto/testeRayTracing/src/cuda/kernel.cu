#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "math.h"

#include <stdio.h>
#include <stdlib.h>

#include "classes.h"
#include "../visualizacao/visualizacao.h"

#include "../include/GL/freeglut.h"

int MAX_THREADSPERBLOCK = 256;

int N_REFLEXOES = 3;
int N_RAIOS = 5;
int N_TRIANGULOS = 4;

Raio *raios_h = NULL, *raios_d = NULL;
Triangulo *triangulos_h = NULL, *triangulos_d = NULL;
Vec3f *posicoesReflexao_d = NULL;
int reflexoes_h[1] = {NULL}, *reflexoes_d = NULL;


__device__ Vec3f::Vec3f() {};

__device__ Vec3f Vec3f::operator-(const Vec3f & b)
{
	Vec3f out;

	out.x = this->x - b.x;
	out.y = this->y - b.y;
	out.z = this->z - b.z;

	return out;
}

__device__ Vec3f Vec3f::operator+(const Vec3f & b)
{
	Vec3f out;

	out.x = this->x + b.x;
	out.y = this->y + b.y;
	out.z = this->z + b.z;

	return out;
}

__device__ Vec3f Vec3f::operator*(const float & b)
{
	Vec3f out;

	out.x = this->x * b;
	out.y = this->y * b;
	out.z = this->z * b;

	return out;
}

__device__ bool sinal(float v) //retorna 1 para numeros positivos e 0, retorna 0 para negativos
{
	if (v >= 0)
		return 1;
	else
		return 0;
}

__device__ Vec3f prodVetorial_d(Vec3f v1, Vec3f v2)
{
	Vec3f out;

	out.x = v1.y*v2.z - v1.z*v2.y;
	out.y = v1.z*v2.x - v1.x*v2.z;
	out.z = v1.x*v2.y - v1.y*v1.x;

	return out;
}

__device__ float prodEscalar_d(Vec3f v1, Vec3f v2)
{
	float out;

	out = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;

	return out;
}

__device__ bool paralelo(Raio raio, Triangulo triangulo)
{
	float prodEsc = prodEscalar_d(raio.dir, triangulo.plano.normal);
	if (prodEsc == 0.0f)
		return true;
	else
		return false;
}

__device__ Vec3f pontoDeContato(Raio raio, Triangulo triangulo)
{
	Vec3f p;

	float d = ((prodEscalar_d(triangulo.a - raio.origem, triangulo.plano.normal)) / prodEscalar_d(triangulo.plano.normal, raio.dir)); //https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
	d = roundf(d * 10e7) / 10e7;
	if (d <= 0) //distancia negativa
	{
		p.x = 6.9814632e+11;
		p.y = 6.9814632e+11;
		p.z = 6.9814632e+11;
	}
	else
	{
		p.x = raio.origem.x + d * raio.dir.x;
		p.y = raio.origem.y + d * raio.dir.y;
		p.z = raio.origem.z + d * raio.dir.z;
	}

	return p;
}
__device__ bool estaDentro(Vec3f p, Triangulo triangulo)
{
	bool out = false;

	bool sinal1 = sinal(prodEscalar_d(prodVetorial_d(triangulo.b - triangulo.a, p - triangulo.a), triangulo.plano.normal));
	bool sinal2 = sinal(prodEscalar_d(prodVetorial_d(triangulo.c - triangulo.b, p - triangulo.b), triangulo.plano.normal));
	bool sinal3 = sinal(prodEscalar_d(prodVetorial_d(triangulo.a - triangulo.c, p - triangulo.c), triangulo.plano.normal));

	if ((sinal1 == sinal2) && (sinal2 == sinal3))
		out = true;

	if (p.x == 6.9814632e+11)
		out = false;

	return out;
}

__device__ float distancia(Vec3f a, Vec3f b)
{
	float dist = sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y) + (b.z - a.z)*(b.z - a.z));

	return dist;
}

__device__ Vec3f direcaoReflexao(Raio raio, Triangulo triangulo)	//direcao = r - 2*N*(N dot dir + d)
{
	Vec3f out;

	out.x = raio.dir.x - 2 * triangulo.plano.normal.x * prodEscalar_d(triangulo.plano.normal, raio.dir);
	out.y = raio.dir.y - 2 * triangulo.plano.normal.y * prodEscalar_d(triangulo.plano.normal, raio.dir);
	out.z = raio.dir.z - 2 * triangulo.plano.normal.z * prodEscalar_d(triangulo.plano.normal, raio.dir);

	return out;
}

__device__ Vec3f direcaoRefracao(Raio raio, Triangulo triangulo, float v1, float v2)	//direcao = (r + N*cos(t1))*v1/v2 - N*sqrt( 1-(v1/v2)^2 * (1-cos^2(t1)) )  --- t1 = angulo de incidencia
{																						//v1 = velocidade no meio atual
	Vec3f out;

	float cos = prodEscalar_d(triangulo.plano.normal, raio.dir);

	float raiz = 1 - (v1 / v2)*(v1 / v2)*(1 - cos*cos);
	if (raiz <= 0) // reflexao total
	{
		out.x = 0;
		out.y = 0;
		out.z = 0;
	}

	else
		out = (raio.dir + triangulo.plano.normal) * (v1 / v2) - triangulo.plano.normal*sqrt(raiz);

	return out;
}

__device__ float intensidadeReflexao(float z1, float z2)	//z1 = impedancia acustica do meio externo
{
	return (((z2 - z1)*(z2 - z1)) / ((z2 + z1) * (z2 + z1)));
}

__global__ void inicia(Raio *raios, int n_raios, Triangulo *triangulos, int n_triangulos, int *qtdeReflexao, int max_reflexoes)
{
	Vec3f origem;
	origem.x = -1;
	origem.y = -1;
	origem.z = -1;



	for (int i = 0; i < max_reflexoes; i++)
	{
		Vec3f pontoReflexao = origem;
		bool ocorreuReflexao = false;
		float menor_distancia = 3.402823e+38;
		Vec3f dirReflexao = origem;
		int ultimo = -1;

		if ((threadIdx.x + blockDim.x*blockIdx.x) < n_raios)
		{
			for (int j = 0; j < n_triangulos; j++)
			{
				//ve se o raio passa em paralelo ao triangulo
				if (!paralelo(raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*i], triangulos[j]))
				{
					//nao passa em paralelo
					//calcula o ponto de contato entre o raio e o triangulo
					Vec3f p = pontoDeContato(raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*i], triangulos[j]);

					if (estaDentro(p, triangulos[j]) && raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*i].ultimo != j)
					{
						//refletiu			
						//calcula a distancia
						float dist = distancia(raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*i].origem, p);
						if (dist < menor_distancia && dist != 0)
						{
							pontoReflexao = p;
							ocorreuReflexao = true;
							dirReflexao = direcaoReflexao(raios[threadIdx.x + blockDim.x * blockIdx.x], triangulos[j]);
							menor_distancia = dist;
							dirReflexao = direcaoReflexao(raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*i], triangulos[j]);
							ultimo = j;
						}
					}

				}
			}
		}

		if (ocorreuReflexao)
		{
			//muda a origem do raio
			raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*(i + 1)].origem = pontoReflexao;
			//muda a direcao
			raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*(i + 1)].dir = dirReflexao;

			raios[threadIdx.x + blockDim.x*blockIdx.x + n_raios*(i + 1)].ultimo = ultimo;
			atomicAdd(qtdeReflexao, 1);
			ocorreuReflexao = false;
			//perde intensidade
			//raios[threadIdx.x + blockIdx.x * blockDim.x].intensidade *= intensidadeReflexao(z1, z2);
		}
	}
}

void desenha()
{
	int i;

	glColor4f(0.8, 0.8, 0.8, 0.6);

	for (i = 0; i < N_TRIANGULOS; i++)
	{
		glBegin(GL_TRIANGLES);
		glVertex3f(triangulos_h[i].a.x, triangulos_h[i].a.y, triangulos_h[i].a.z);
		glVertex3f(triangulos_h[i].b.x, triangulos_h[i].b.y, triangulos_h[i].b.z);
		glVertex3f(triangulos_h[i].c.x, triangulos_h[i].c.y, triangulos_h[i].c.z);
		glEnd();
	}

	glLineWidth(2.5);
	glColor4f(0.0, 1.0, 0.0, 0.8);

	for (i = 0; i < N_RAIOS - 1; i++)
	{
		if (raios_h[i].origem.x == 0 && raios_h[i].origem.y == 0 && raios_h[i].origem.z == 0 &&
			raios_h[i].dir.x == -1 && raios_h[i].dir.y == 0 && raios_h[i].dir.z == 0)							//se o raio nao existe
			continue;

		else if (raios_h[i + 1].origem.x == 0 && raios_h[i + 1].origem.y == 0 && raios_h[i + 1].origem.z == 0 &&
			raios_h[i + 1].dir.x == -1 && raios_h[i + 1].dir.y == 0 && raios_h[i + 1].dir.z == 0)						//se o proximo raio nao existe
		{
			glBegin(GL_LINES);
			glVertex3f(raios_h[i].origem.x, raios_h[i].origem.y, raios_h[i].origem.z);
			glVertex3f(raios_h[i].origem.x + raios_h[i].dir.x * 8, raios_h[i].origem.y + raios_h[i].dir.y * 8, raios_h[i].origem.z + raios_h[i].dir.z * 8);
			glEnd();
			continue;
		}
		glBegin(GL_LINES);
		glVertex3f(raios_h[i].origem.x, raios_h[i].origem.y, raios_h[i].origem.z);
		glVertex3f(raios_h[i + 1].origem.x, raios_h[i + 1].origem.y, raios_h[i + 1].origem.z);
		glEnd();
	}

	glBegin(GL_LINES);
	glVertex3f(raios_h[i].origem.x, raios_h[i].origem.y, raios_h[i].origem.z);
	glVertex3f(raios_h[i].origem.x + 10, raios_h[i + 1].origem.y + 10, raios_h[i + 1].origem.z + 10);
	glEnd();
}


void renderScene(void)
{

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();

	computePos(dist);

	// Set the camera

	gluLookAt(x, y, z,
		lx, ly, lz,
		sin(phi), cos(phi), 1);

	desenha();

	glutSwapBuffers();
}


int main(int argc, char **argv)
{
	raios_h = (Raio*)malloc(N_RAIOS*(N_REFLEXOES + 1) * sizeof(Raio));
	triangulos_h = (Triangulo*)malloc(N_TRIANGULOS * sizeof(Triangulo));

	cudaMalloc((void**)&raios_d, sizeof(Raio)*(N_RAIOS*(N_REFLEXOES+1)));
	cudaMalloc((void**)&triangulos_d, sizeof(Triangulo)*N_TRIANGULOS);
	cudaMalloc((void**)&reflexoes_d, sizeof(int));


	raios_h = (Raio*) malloc(N_RAIOS * sizeof(Raio));

	Vec3f origem = Vec3f(0, 0, 0);
	Vec3f direcao = Vec3f(1, 1, 0);


	//criando os raios
	raios_h[0] = Raio(origem, direcao);			// dir = (1,1,0) reflete x2
	direcao = Vec3f(1, 0, 0);
	raios_h[1] = Raio(origem, direcao);			// dir = (1,0,0) reflete
	direcao = Vec3f(2, 1, 0);
	raios_h[2] = Raio(origem, direcao);			// dir = (2,1,0) reflete x2
	direcao = Vec3f(0, 1, 1);
	raios_h[3] = Raio(origem, direcao);			// dir = (0,1,1)
	origem = Vec3f(1, 1, 1);
	direcao = Vec3f(1, 1, 1);
	raios_h[4] = Raio(origem, direcao);			// dir = (1,1,1)


	direcao = Vec3f(-1, 0, 0);

	

	for (int i = 5; i < (N_RAIOS*(N_REFLEXOES+1)); i++)
	{
		raios_h[i] = Raio(origem,direcao);
	}


	//criando os triangulos
	Vec3f a = Vec3f(5, 0, -5);
	Vec3f b = Vec3f(5, 0, 5);
	Vec3f c = Vec3f(5, 5, 0);

	triangulos_h[0] = Triangulo(a, b, c);

	a = Vec3f(0, 4, 5);
	b = Vec3f(0, 4, -5);
	c = Vec3f(0, 10, 0);

	triangulos_h[1] = Triangulo(a, b, c);

	a = Vec3f(4, 0, -5);
	b = Vec3f(4, 0, 5);
	c = Vec3f(4, 5, 0);

	triangulos_h[2] = Triangulo(a, b, c);

	a = Vec3f(4, 11, -5);
	b = Vec3f(4, 11, 5);
	c = Vec3f(4, 17, 0);

	triangulos_h[3] = Triangulo(a, b, c);


	reflexoes_h[0] = 0;

	cudaMemcpy(raios_d, raios_h, N_RAIOS*(N_REFLEXOES+1) * sizeof(Raio), cudaMemcpyHostToDevice);
	cudaMemcpy(triangulos_d, triangulos_h, N_TRIANGULOS * sizeof(Triangulo), cudaMemcpyHostToDevice);
	cudaMemcpy(reflexoes_d, reflexoes_h, sizeof(int), cudaMemcpyHostToDevice);

	inicia << <MAX_THREADSPERBLOCK-1 + N_RAIOS, MAX_THREADSPERBLOCK >> > (raios_d, N_RAIOS, triangulos_d, N_TRIANGULOS, reflexoes_d, N_REFLEXOES);

	cudaMemcpy(reflexoes_h, reflexoes_d, sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(raios_h, raios_d, N_RAIOS*(N_REFLEXOES+1) * sizeof(Raio), cudaMemcpyDeviceToHost);

	int r = reflexoes_h[0];

	printf("Reflexoes: %d\n", r);
	for (int i = 0; i < (N_RAIOS*(N_REFLEXOES+1)); i++)
	{
		if (!(i%N_RAIOS))
			printf("------------------------------------------------------------------------------------------------------\n");
		printf("(%f, %f, %f) dir: (%f, %f, %f)\n",raios_h[i].origem.x, raios_h[i].origem.y, raios_h[i].origem.z, raios_h[i].dir.x, raios_h[i].dir.y, raios_h[i].dir.z);
	}

	cudaFree(raios_d);
	cudaFree(triangulos_d);
	cudaFree(reflexoes_d);

	// init GLUT and create Window
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1200, 800);
	glutCreateWindow("testeOpenGL");

	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);
	glutSpecialFunc(pressKey);
	glutSpecialUpFunc(releaseKey);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMove);

	// enter GLUT event processing cycle
	glutMainLoop();

    return 0;
}
