#include "visualizacao.h"
#include <math.h>
#include "../include/GL/freeglut.h"

#include <stdio.h>

#define GLUT_DISABLE_ATEXIT_HACK

void renderScene();
void changeSize(int w, int h);
void pressKey(int key, int x, int y);
void releaseKey(int key, int x, int y);
void mouseButton(int button, int state, int xx, int yy);
void mouseMove(int xx, int yy);
void computePos(float dist);

// posicao da camera em coordenadas esfericas
float r = 100, theta = 0, phi = 0;
// direcao da camera
float lx, ly, lz;
// posicao da camera
float x = -10.0f, y = 10.0f, z = -10.0f;

float xOrigin = -1, yOrigin = -1;

float dTheta = 0, dPhi = 0;
float dist = 0;



void changeSize(int w, int h) 
{

	// Prevent a divide by zero, when window is too short
	if (h == 0)
		h = 1;
	float ratio = 1.0* w / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45, ratio, 1, 1000);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void pressKey(int key, int x, int y)
{

	switch (key)
	{
	case GLUT_KEY_UP: dPhi = -0.1f; break;
	case GLUT_KEY_DOWN: dPhi = +0.1f; break;
	case GLUT_KEY_LEFT: dTheta = 0.1f; break;
	case GLUT_KEY_RIGHT: dTheta = -0.1f; break;
	}
}

void releaseKey(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP: dPhi = 0; break;
	case GLUT_KEY_DOWN: dPhi = 0; break;
	case GLUT_KEY_LEFT: dTheta = 0; break;
	case GLUT_KEY_RIGHT: dTheta = 0; break;
	}
}

void mouseButton(int button, int state, int xx, int yy) 
{
	if (button == GLUT_LEFT_BUTTON)
	{

		// when the button is released
		if (state == GLUT_UP)
		{
			dTheta = 0;
			dPhi = 0;
		}

		else
		{
			xOrigin = xx;
			yOrigin = yy;
		}
	}
}

void mouseMove(int xx, int yy)
{
	if (xOrigin > 0)
	{
		dTheta = (xx - xOrigin) * 0.001f;
	}
	else
	{
		dTheta = 0;
	}

	if (yOrigin > 0)
	{
		dPhi = (yy - yOrigin) * 0.001f;
	}
	else
	{
		dPhi = 0;
	}
}

void computePos(float deltaMove)
{
	theta += dTheta;
	if (theta >= 3.1415)
	{
		theta = 3.1415;
	}

	else if (theta < -3.1415)
	{
		theta = -3.1415;
	}

	phi += dPhi;
	if (phi > 3.1415)
		phi = 3.1415;
	if (phi < -3.1415)
		phi = -3.1415;

	r += dist*3.0f;
	if (r < 8)
		r = 8;

	x = r * sin(theta) * cos(phi);
	y = r * sin(theta) * sin(phi);
	z = r * cos(theta);

	lx = -x;
	ly = -y;
	lz = -z;
}

void glutStart()
{
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1200, 800);

	glutDisplayFunc(&renderScene);
	printf("Display func set\n");
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);
	glutSpecialFunc(pressKey);
	glutSpecialUpFunc(releaseKey);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMove);

	printf("vou criar a janela");
	getchar();
	glutCreateWindow("raytracing");
	getchar();
	glutMainLoop();
	getchar();

	return;
}