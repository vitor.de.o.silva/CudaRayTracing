#ifndef vis_H
#define vis_H
#include "..\cuda\classes.h"

// posicao da camera em coordenadas esfericas
extern float r , theta , phi;
// direcao da camera
extern float lx, ly, lz;
// posicao da camera
extern float x, y, z;

// variacao da posicao do mouse
extern float xOrigin, yOrigin;

extern float dTheta, dPhi;
extern float dist;

void desenha();
void renderScene(void);
void changeSize(int w, int h);
void pressKey(int key, int x, int y);
void releaseKey(int key, int x, int y);
void mouseButton(int button, int state, int xx, int yy);
void mouseMove(int xx, int yy);
void computePos(float deltaMove);

void glutStart();

#endif